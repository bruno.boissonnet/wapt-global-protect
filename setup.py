# -*- coding: utf-8 -*-
from setuphelpers import *

r"""
Usable WAPT package functions: install(), uninstall(), session_setup(), audit(), update_package()

"""
# Declaring global variables - Warnings: 1) WAPT context is only available in package functions; 2) Global variables are not persistent between calls


def install():
    print(u'Installation en cours de Global Protect ...')
    # Declaring local variables
    properties = {'PORTAL':'acces.intranet.inrae.fr'}

    # Installing the software
    print("Installation de : Global Protect version 6.0.5-30.")
    install_msi_if_needed('GlobalProtect64.msi',properties = properties )
    print(u'Installation terminée.')



