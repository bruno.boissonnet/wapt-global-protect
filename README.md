# WAPT-Global_Protect

Paquet [WAPT](https://www.tranquil.it/gerer-parc-informatique/decouvrir-wapt/) pour l'installation silencieuse de Global Protect.


## Paquet GlobalProtect

Ce paquet installe la dernière version de Palo Alto Global Protect de manière silencieuse.
Lors de l'installation, Global Protect est configuré pour utiliser le portail INRAE : acces.intranet.inrae.fr

Dernière version du logiciel Global Protect: 6.0.5-30

## Utiliser ce paquet

Pour utiliser ce paquet, il faut : 

1. Télécharger tous les fichiers
2. Ouvrir la console WAPT
3. Générer un modèle de paquet
4. Pointer sur les fichiers téléchargés

